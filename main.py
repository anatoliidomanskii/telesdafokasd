from aiogram import Bot, Dispatcher, types
from aiogram.utils import executor
import openai_secret_manager
import openai


TOKEN_TELEGRAM_API = "YOUR TOKEN"
TOKEN_OPENAI_API = "YOUR TOKEN"

# Set up the Telegram bot
bot = Bot(TOKEN_TELEGRAM_API)
dispatcher = Dispatcher(bot)

# Set up the OpenAI API
openai.api_key = TOKEN_OPENAI_API


async def process_message(message: types.Message):
    try:
        # Call the OpenAI API to generate a response
        response = openai.Completion.create(
            engine="davinci",
            prompt=message.text,
            max_tokens=1024,
            n=1,
            stop=None,
            temperature=0.7,
        )
        # Send the response back to the user
        await message.answer(response.choices[0].text)
    except Exception as e:
        await message.answer("Sorry, there was an error processing your request.")


dispatcher.register_message_handler(process_message)

if __name__ == '__main__':
    executor.start_polling(dispatcher, skip_updates=True)